package Hotel;

public class Room {

    private int roomNumber;
    private boolean roomEmpty;
    private String guestName;


    Room(int roomNumber, boolean roomEmpty, String guestName){
        this.roomNumber = roomNumber;
        this.roomEmpty = roomEmpty;
        this.guestName = guestName;
    }
    public int getRoomNumber(){
        return roomNumber;
    }
    public boolean isRoomEmpty(){
        return roomEmpty;
    }
    public String getGuestName(){
        return guestName;
    }
    public void setRoomEmpty(boolean status){
        // True if empty, false if full
        roomEmpty = status;
    }
    public void setGuestName(String guestName){
        this.guestName = guestName;
     }
}
