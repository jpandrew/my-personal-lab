package Hotel;

public class Main {

    public static void main(String[] args) {

        Hotel hotel = new Hotel ();
        Client zecarlos = new Client("Zé Carlos", hotel);
        Client ruimoreira = new Client("Rui Moreira", hotel);
        Client ricardo = new Client("Ricardo Simoes", hotel);
        Client bernardo = new Client("Bernardo Monteiro", hotel);



        zecarlos.checkIn("Zé Carlos");
        ruimoreira.checkIn("Rui Moreira");
        ricardo.checkIn("Ricardo");
        bernardo.checkIn("bernardo");
        zecarlos.checkOut(401);
        bernardo.checkOut(0);
        ruimoreira.checkOut("Rui Moreira");
        bernardo.checkIn("Bernardo Monteiro");
        bernardo.checkIn("Bernardo Monteiro");
    }
}
