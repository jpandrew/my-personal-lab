package Hotel;

public class Hotel {


    private Room[] rooms = new Room[3];

    public Hotel() {

        rooms[0] = new Room(401, true, "");
        rooms[1] = new Room(402, true, "");
        rooms[2] = new Room(403, true, "");
    }

    public int setRoomFull(String guestName) {
        int checkEmpty = checkEmpty();
        if (checkEmpty != -1) {
            rooms[checkEmpty].setRoomEmpty(false);
            rooms[checkEmpty].setGuestName(guestName);

            return rooms[checkEmpty].getRoomNumber();
        } else {
            return -1;
        }
    }

    public void setRoomEmpty(int roomNumber) {
        int search = search(rooms, roomNumber);
        if (search != -1) {
            rooms[search].setRoomEmpty(true);
            System.out.println("Check-out has been completed with success. Thank you for your staying " + rooms[search].getGuestName() + ".");
            rooms[search].setGuestName("");
        } else {
            System.out.println("Are you sure you are a guest at this hotel?");
        }
    }

    public void setRoomEmpty(String guestName) {
        int search = search(rooms, guestName);
        if (search != -1) {
            rooms[search].setRoomEmpty(true);
            System.out.println("Check-out has been completed with success. Thank you for your staying " + rooms[search].getGuestName() + ".");
            rooms[search].setGuestName("");
        } else {
            System.out.println("Are you sure you are a guest at this hotel?");
        }
    }

    public int checkEmpty() {
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i].isRoomEmpty()) {
                return i;
            }
        }
        return -1;
    }

    public int search(Room[] rooms, int roomNumber) {
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i].getRoomNumber() == roomNumber) {
                return i;
            }
        }
        return -1;
    }

    public int search(Room[] rooms, String guestName) {
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i].getGuestName().equals(guestName)) {
                return i;
            }
        }
        return -1;
    }
}
