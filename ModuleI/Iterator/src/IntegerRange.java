import java.util.Iterator;

class IntegerRange implements Iterable<Integer> {

    private Integer min;
    private Integer max;
    private boolean reversed;
    private int position;


    public IntegerRange(Integer min, Integer max) {
        this.min = min;
        this.max = max;
    }



    public void setReversed(boolean reversed) {
        this.reversed = reversed;
            if(reversed){
                position = max + 1;
            }
            else {
                position = min - 1;
            }
    }

    public Iterator<Integer> iterator() {

        return new Iterator() {
            @Override
            public boolean hasNext() {
                if (reversed) {
                    return position > min;
                } else {
                    return position < max;
                }
            }

            @Override
            public Integer next() {
                if (reversed) {
                    position--;
                } else {
                    position++;
                }
                return position;
            }
        };
    }
}
