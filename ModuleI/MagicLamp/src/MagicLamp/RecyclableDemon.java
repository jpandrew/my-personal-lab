package MagicLamp;

public class RecyclableDemon extends Genie {

    private boolean isRecycled;
    private MagicLamp magicLamp;

    public RecyclableDemon(int wishesAvailable, int wishCount, String genieMessage, MagicLamp magicLamp) {
        super(wishesAvailable, wishCount, genieMessage);
        this.magicLamp = magicLamp;
        isRecycled = false;

    }

    @Override
    public String makeWish() {
        if (!isRecycled) {
            super.makeWish();
            return "YOUR WISH IS GRANTED MORTAL. LET ME REMIND YOU STILL HAVE UNLIMITED WISHES";
        }
        return "Your demon king doesn't exist. Are you sure you didn't recycle it?";
    }

    public String recycleDemon() {
        if (!isRecycled) {
            isRecycled = true;
            magicLamp.setGeniesReleased(0);
            magicLamp.setRechargedTimes(1);
            return "Your demon has been recycled! Thank you for your service!";

        }
        return "You cannot recycle a the Demon King at this moment.";
    }

}
