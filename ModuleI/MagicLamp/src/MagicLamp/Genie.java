package MagicLamp;

public class Genie {

    private int wishesAvailable;
    private int wishCount;
    private String genieMessage = "";

    public Genie(int wishesAvailable, int wishCount, String genieMessage) {
        this.wishesAvailable = wishesAvailable;
        this.wishCount = wishCount;
        this.genieMessage = genieMessage;
    }

    public int getWishesAvailable() {
        return wishesAvailable;
    }

    public int getWishCount() {
        return wishCount;
    }

    public void setWishesAvailable(int wishesAvailable) {
        this.wishesAvailable = wishesAvailable;
    }

    public void setWishCount(int wishCount) {
        this.wishCount = wishCount;
    }

    public String makeWish() {

            wishesAvailable--;
            ++wishCount;
            return "Your wish is granted";
    }
}
