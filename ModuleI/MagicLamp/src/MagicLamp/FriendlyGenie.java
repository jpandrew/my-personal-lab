package MagicLamp;

public class FriendlyGenie extends Genie {

    public FriendlyGenie(int maxWishes, int wishCount, String genieMessage) {
        super(maxWishes, wishCount, genieMessage);
    }

    @Override
    public String makeWish() {
        if (super.getWishesAvailable() > 0) {

            return super.makeWish();
        }
        return "I am not capable of making more wishes";
    }
}
