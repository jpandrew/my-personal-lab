package MagicLamp;

public class MagicLamp {

    private int maxGenieRelease = 5;
    private int rechargedTimes = 0;
    private int geniesReleased = 0;
    private int totalGeniesReleased;
    private Genie currentGenie;


    public Genie rubLamp(){
        int number = (int) Math.ceil(Math.random()*10);
        if(geniesReleased == 4){
            currentGenie = new RecyclableDemon(3,0,"",this);
            totalGeniesReleased++;
            geniesReleased++;
            System.out.println("YOU HAVE SUMMONED THE GREAT DEMON KING OF ARGAVAL. THANK YOU FOR FREEING ME MORTAL. I SHALL FULFILL ANY WISH YOU MAY MAKE");
        }
        else if(maxGenieRelease == geniesReleased) {
            System.out.println("No more genies can be released. Please charge batteries by recycling your current demon.");
        }
        else if(maxGenieRelease > 0){
            if((number%2) == 0) {
                currentGenie = new FriendlyGenie(3,0, "");
                geniesReleased++;
                totalGeniesReleased++;
                System.out.println("Heeeeeeeey buddy, I can fulfill your wishes! :D");
            }
            else{
                currentGenie = new GrumpyGenie(3,0,"");
                geniesReleased++;
                totalGeniesReleased++;
                System.out.println("I might fulfill your wishes, if I feel like it, idiot...");
            }
        }
        return currentGenie;
    }

    public int getMaxGenieRelease(){
        return maxGenieRelease;
    }
    public int getGeniesRelease(){
        return geniesReleased;
    }
    public int getRechargedTimes(){
        return rechargedTimes;
    }
    public void setRechargedTimes(int number){
        rechargedTimes = rechargedTimes + number;
    }
    public void setGeniesReleased(int number){
        geniesReleased = number;
    }


    @Override
    public String toString() {
        return "Magic Lamp {" +
                "maxGenieRelease = " + maxGenieRelease +
                ", rechargedTimes = " + rechargedTimes +
                ", geniesReleased = " + geniesReleased +
                ", totalGeniesReleased = " + totalGeniesReleased + "}";
    }

}
