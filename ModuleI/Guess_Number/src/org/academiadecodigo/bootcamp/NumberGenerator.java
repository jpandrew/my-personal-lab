package org.academiadecodigo.bootcamp;

public class NumberGenerator {

    public static int randomNumber(int maxGuess) {

        int guess = (int) Math.ceil(Math.random() * maxGuess);

        return guess;
    }
}
