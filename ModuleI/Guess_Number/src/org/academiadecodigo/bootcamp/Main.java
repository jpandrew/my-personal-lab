package org.academiadecodigo.bootcamp;

public class Main {

    public static void main(String[] args) {

        Player playerZe = new Player("Zé Carlos");
        Player playerTo = new Player("Antó");
        Player playerRolin = new Player("Rolin");

        Player[] players = {playerZe, playerTo, playerRolin};


        Game game = new Game(10, players);
        game.start();
    }
}

