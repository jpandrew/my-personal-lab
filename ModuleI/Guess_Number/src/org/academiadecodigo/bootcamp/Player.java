package org.academiadecodigo.bootcamp;

public class Player {

    private String name;

    public Player(String name) {

        this.name = name;
    }

    public int guessNumber(int maxGuess) {

        int guess = NumberGenerator.randomNumber(maxGuess);
        System.out.println("Player " + name + ": My guess is " + guess);

        return guess;
    }

    public String getName() {
        return name;
    }
}
