package org.academiadecodigo.bootcamp.car;

import org.academiadecodigo.bootcamp.grid.position.GridPosition;

public class Ambulance extends Car {

    public final static int AMBSPEED = 2;
    private boolean crashed = false;

    public Ambulance(GridPosition pos) {
        super(pos, CarType.AMBULANCE);
    }

    @Override
    public void move() {
        accelerate(chooseDirection(), 2);
    }

    @Override
    public void crash() {
         crashed = false;
    }

    @Override
    public String toString() {
        return "Ambulance{" +
                "crashed=" + crashed +
                ", collisionDetector=" + collisionDetector +
                ", currentDirection=" + currentDirection +
                '}';
    }
}
