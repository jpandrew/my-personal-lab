package org.academiadecodigo.bootcamp.car;

import org.academiadecodigo.bootcamp.grid.GridDirection;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;

public class UserCar extends Car{

    public final static int SPEED = 1;

    public UserCar(GridPosition pos){
        super(pos, CarType.MYCAR);
    }

    @Override
    public void move() {
    }
    public void moveDown() {
        moveCar(GridDirection.DOWN, 1);
    }

    public void moveUp() {
        moveCar(GridDirection.UP, 1);
    }

    public void moveLeft() {
        moveCar(GridDirection.LEFT, 1);
    }

    public void moveRight() {
        moveCar(GridDirection.RIGHT, 1);
    }

    public void moveCar(GridDirection direction, int SPEED) {
        if (isCrashed()) {
            return;
        }
        else {
            getPos().moveInDirection(direction, SPEED);
        }
    }
}
