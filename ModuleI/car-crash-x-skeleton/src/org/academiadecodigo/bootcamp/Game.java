package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.car.Car;
import org.academiadecodigo.bootcamp.car.CarFactory;
import org.academiadecodigo.bootcamp.car.UserCar;
import org.academiadecodigo.bootcamp.gfx.simplegfx.SimpleGfxGrid;
import org.academiadecodigo.bootcamp.gfx.simplegfx.SimpleGfxLine;
import org.academiadecodigo.bootcamp.grid.Grid;
import org.academiadecodigo.bootcamp.grid.GridFactory;
import org.academiadecodigo.bootcamp.grid.GridType;
import org.academiadecodigo.bootcamp.handler.Controller;
import org.academiadecodigo.simplegraphics.graphics.Line;

/**
 * The game logic
 */
public class Game {


    /**
     * Graphical Car field
     */
    private Grid grid;

    /**
     * Container of Cars
     */
    private Car[] cars;

    /**
     * Animation delay
     */
    private int delay;

    /**
     * The collision detector
     */
    private CollisionDetector collisionDetector;

    /**
     * Number of cars to manufacture
     */
    private int manufacturedCars = 20;

    Controller controls = new Controller();
    private SimpleGfxLine line;


    /**
     * Constructs a new game
     * @param gridType which grid type to use
     * @param cols number of columns in the grid
     * @param rows number of rows in the grid
     * @param delay animation delay
     */
    Game(GridType gridType, int cols, int rows, int delay) {

        grid = GridFactory.makeGrid(gridType, cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

       /* line = new SimpleGfxLine(5,5, (SimpleGfxGrid) grid);*/

        grid.init();
       /* line.show();*/


        cars = new Car[manufacturedCars+1];
        collisionDetector = new CollisionDetector(cars);
        cars[cars.length-1] = new UserCar(grid.makeGridPosition());
        cars[cars.length-1].setCollisionDetector(collisionDetector);
        cars[cars.length-1].setGrid(grid);
        controls.init((UserCar) cars[cars.length-1]);
        controls.setCar((UserCar) cars[cars.length-1]);

        for (int i = 0; i < manufacturedCars; i++) {

            cars[i] = CarFactory.getNewCar(grid);
            cars[i].setCollisionDetector(collisionDetector);
            cars[i].setGrid(grid);
            System.out.println(cars[i]);

        }

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);
            collisionDetector.check(cars[cars.length-1]);
            moveAllCars();

        }

    }

    /**
     * Moves all cars
     */
    public void moveAllCars() {

        for (int i = 0; i < manufacturedCars; i++) {
            cars[i].move();
            collisionDetector.check(cars[i]);
        }

    }

}
