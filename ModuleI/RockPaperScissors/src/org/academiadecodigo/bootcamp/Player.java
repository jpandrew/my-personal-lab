package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.enums.Weapon;

public class Player {
    private final String name;
    public int totalWins;

    public Player(String name) {

        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public Weapon useWeapon() {
        Weapon[] options = Weapon.values();
        int index = (int) Math.floor(Math.random() * options.length);

        return options[index];
    }
}

