package org.academiadecodigo.bootcamp;

public class Main {
    public static void main(String[] args) {
        Player player1 = new Player("Ricardo");
        Player player2 = new Player("José");
        GameEngine gameMaster = new GameEngine(player1, player2);
        System.out.println(gameMaster.start());

    }
}
