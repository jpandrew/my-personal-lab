package Exceptions;

public class NotEnoughPermissionsException extends Exception{

    public NotEnoughPermissionsException(){
        super("Not enough permissions");
    }
}
