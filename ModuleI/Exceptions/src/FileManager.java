import Exceptions.FileNotFoundException;
import Exceptions.NotEnoughPermissionsException;
import Exceptions.NotEnoughSpaceException;

public class FileManager {

    boolean login;
    private File file;
    private String userName;

    public FileManager() {
        this.login = false;
        this.file = null;
        this.userName = null;
    }

    public void login(String name){
        if(null == userName){
            setLogin(true);
            userName = name;
            System.out.println("Thank you for coming " + userName);
        }
        else {
            System.out.println("You are already logged in");
        }
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public boolean isLogin() {
        return login;
    }

    public void logout(){
        if(isLogin()){
            setLogin(false);
        }
        else {
            System.out.println("Not logged in");
        }
    }
    public void searchFile(String name) throws FileNotFoundException {
        if (!file.getFilename().equals(name)) {
            throw new FileNotFoundException();
        }
        else {
            System.out.println("File found");
        }
    }

    public void createFile(String name) throws NotEnoughSpaceException, NotEnoughPermissionsException {
        if(!isLogin()){
            throw new NotEnoughPermissionsException();
        }
        if(null == file){
            file = new File(name);
            System.out.println("File " + name + " created");
        }
        else {
            throw new NotEnoughSpaceException();
        }
    }
}
