package org.academiadecodigo.floppybirds;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Character {

    private static final int DISTANCE = 10;
    private Picture picture;
    private Picture blast;
    private boolean moving = false;
    private double x;
    private double y;

    public void setMoving(boolean moving) {
        this.moving = true;
    }

    public boolean isMoving() {
        return moving;
    }

    public Character() {
        picture = new Picture(-50, 50, "resources/resting.png");
        blast = new Picture(-50, 50, "resources/blast-final.png");
    }

    public void render() {
        picture.grow(80, 100);
        blast.grow(240, 160);

        picture.draw();
    }


    public void moveDown() {
        moveAllSprites(0, DISTANCE);
    }

    public void moveUp() {
        moveAllSprites(0, -DISTANCE);
    }

    public void moveLeft() {
        moveAllSprites(-DISTANCE, 0);
    }

    public void moveRight() {
        moveAllSprites(DISTANCE, 0);
    }

    public void move() {
        System.out.println("falling");
        if (!moving) {
            for (int i = 0; i < DISTANCE; i++) {
                x = 0;
                y = (i / 8) * DISTANCE;
                if (true/*placeholder for position limit*/){
                    picture.translate(x, y);
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else{
                    x = 0;
                    y = 0;
                    picture.translate(x, y);
                }

            }
            if (moving) {
                for (int i = 0; i < DISTANCE; i++) {
                    x = 0;
                    y = (-i / 8) * DISTANCE;
                    if (true/*placeholder for position limit*/){
                        picture.translate(x, y);
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else{
                        x = 0;
                        y = 0;
                        picture.translate(x, y);
                    }
                }
                moving = false;
            }
        }
    }

    public void jump() {
        System.out.println("jumping");
        if (moving) {

            for (int i = 0; i < DISTANCE; i++) {
               /* x = i * (Math.PI / 8);
                y = Math.sin(x*10);*/
                x = i / 4;
                y = -i / 2;
                picture.translate(x, y);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            moving = false;
        }
    }

    public void moveAllSprites(double x, double y) {
        Picture[] sprites = {picture, blast};
        for (Picture character : sprites) {
            character.translate(x, y);
        }
    }

}

