package org.academiadecodigo.floppybirds;

public class Main extends Thread {
    public static void main(String[] args) {

        //setup
        Scene scene = new Scene();

        Character character = new Character();
        Controls controls = new Controls();
        controls.setCharacter(character);

        //drawing stuff on screen
        scene.render();
        character.render();


        //hand control of the character to player
        controls.init();
        while (true) {
            character.move();
        }
    }
}

