package org.academiadecodigo.floppybirds;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Scene {

    public void render(){

        Picture background = new Picture(-140, -90, "resources/world-tournament.jpg");

        //this works but may introduce bugs, using an image with the desired size is recommended
        background.grow(-150, -100);

        background.draw();
    }
}
