package rsimoes.player;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import rsimoes.player.handler.InputHandler;
import rsimoes.player.handler.KeyboardManager;

public class Main {
    public static void main(String[] args) {

       MapEditor mapEditor = new MapEditor(20,20);
      InputHandler inputHandler = new InputHandler(mapEditor);
        KeyboardManager.initKeyboard(inputHandler);
    }

}
