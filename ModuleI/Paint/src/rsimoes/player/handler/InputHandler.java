package rsimoes.player.handler;

import rsimoes.player.MapEditor;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import static rsimoes.player.MiddleMan.Direction.*;


public class InputHandler implements KeyboardHandler {

    private MapEditor editor;

    public InputHandler(MapEditor editor) {
        this.editor = editor;
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_RIGHT:
                editor.moveCursor(RIGHT);
                break;
            case KeyboardEvent.KEY_LEFT:
                editor.moveCursor(LEFT);
                break;
            case KeyboardEvent.KEY_DOWN:
                editor.moveCursor(DOWN);
                break;
            case KeyboardEvent.KEY_UP:
                editor.moveCursor(UP);
                break;
            case KeyboardEvent.KEY_SPACE:
                editor.setPainting(true);
                editor.paintUnit();
                break;
            case KeyboardEvent.KEY_C:
                editor.clear();
                break;
            case KeyboardEvent.KEY_S:
                editor.save();
                break;
            case KeyboardEvent.KEY_L:
                editor.load();
                break;
            case KeyboardEvent.KEY_E:
                System.exit(0);
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            editor.setPainting(false);
        }

    }
}

