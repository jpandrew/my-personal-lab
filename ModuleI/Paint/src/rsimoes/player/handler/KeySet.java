package rsimoes.player.handler;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;

public enum KeySet {

    UP(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_UP),
    DOWN(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_DOWN),
    LEFT(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_LEFT),
    RIGHT(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_RIGHT),
    SPACE(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_SPACE),
    C(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_C),
    S(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_S),
    L(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_L),
    E(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_E);

    public final KeyboardEventType TYPE;
    public final int KEY_NUM;

    KeySet(KeyboardEventType TYPE, int KEY_NUM){
        this.TYPE = TYPE;
        this.KEY_NUM = KEY_NUM;
    }

}
