package rsimoes.player.grid;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import static rsimoes.player.grid.Grid.*;


public class Unit {

    protected int row;
    protected int col;
    protected Rectangle rectangle;
    protected boolean isPainted;

    public Unit(int col, int row) {
        this.row = row;
        this.col = col;
        rectangle = new Rectangle(col * CELL_SIZE + PADDING, row * CELL_SIZE + PADDING, CELL_SIZE, CELL_SIZE);
        this.rectangle.draw();
    }

    public void hide() {
        this.rectangle.draw();
        this.isPainted = false;
    }

    public void paint() {
        this.rectangle.fill();
        this.isPainted = true;
    }

    public boolean isPainted() {
        return isPainted;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

}
