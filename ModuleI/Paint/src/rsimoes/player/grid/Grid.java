package rsimoes.player.grid;

import rsimoes.player.MiddleMan;

import java.io.*;

public class Grid {

    public static final int PADDING = 30;
    public static final int CELL_SIZE = 30;

    private final int rows;
    private final int cols;

    private final Unit[][] field;
    private MiddleMan middleMan;
    private DataOutputStream dataOut;
    private DataInputStream dataIn;

    public Grid(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;
        field = new Unit[cols][rows];
        initField();
    }

    public void initField() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = new Unit(i, j);
            }
        }
    }

    public void clear() {
        for (Unit[] units : field) {
            for (Unit unit : units) {
                unit.hide();
            }
        }
    }

    public Unit getCell(int row, int col) {
        return field[col][row];
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }


    public void save() {
        String fileName = "./resources/SaveFile";
        try {
            FileOutputStream outputStream = new FileOutputStream(fileName);
            dataOut = new DataOutputStream(outputStream);

            for (Unit[] units : field) {
                for (Unit unit : units) {

                    dataOut.writeBoolean(unit.isPainted());
                }
            }

            dataOut.flush();
            System.out.println("Game file saved successfully!");

        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            cleanUp(dataOut);
        }

    }

    public void load() {
        String fileName = "./resources/SaveFile";
        try {
            FileInputStream inputStream = new FileInputStream(fileName);
            dataIn = new DataInputStream(inputStream);

            for (Unit[] units : field) {
                for (Unit unit : units) {
                    if (dataIn.readBoolean()) {
                        unit.paint();
                    }
                }
            }
            System.out.println("Game file loaded successfully!");
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            cleanUp(dataIn);
        }

    }

    private void cleanUp(InputStream file) {

        try {
            if(dataIn != null) {
                file.close();
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void cleanUp(OutputStream file) {

        try {
            if(dataOut != null) {
                file.close();
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

}
