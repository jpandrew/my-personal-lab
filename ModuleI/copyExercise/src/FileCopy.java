import java.io.*;

public class FileCopy {

    FileInputStream inputStream = null;
    FileOutputStream outputStream = null;


    public void start(String src, String dest) {
        
        try {
            inputStream = new FileInputStream(src);
            outputStream = new FileOutputStream(dest);

            inputStream.transferTo(outputStream);

            //Code below is the manual way. Code above is Java 9 and above
            /*byte[] buffer = new byte[1024];
           int len;
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);

            }*/
            inputStream.close();
            outputStream.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            cleanUp();
        }
    }

    public void cleanUp() {
        try {
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


