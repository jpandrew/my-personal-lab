import java.util.*;

public class WordHistogram extends HashMap<String, Integer> implements Iterable<String> {


    public WordHistogram(String string) {
        String[] spliced = string.split("\\s+");
        for (String word : spliced) {
            if(containsKey(word)) {
                put(word, get(word) + 1);
            }
            else {
                put(word, 1);
            }
        }
    }

    public Iterator<String> iterator() {
        return keySet().iterator();
    }
    
}
