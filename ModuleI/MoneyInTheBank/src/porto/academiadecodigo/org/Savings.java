package porto.academiadecodigo.org;

public class Savings {

    private float balance;

    public Savings(float balance) {

        this.balance = balance;
    }

    public float getBalance() {
        return balance;
    }

    public String checkBalance() {
        return "Your savings balance is " + balance;
    }

    public boolean operationPossible(float value) {
        boolean goAhead;
        if (value <= balance) {
            return true;
        } else {
            return false;
        }
    }

    public void depositBalance(float newBalance) {
        balance = balance + newBalance;
    }

    public void withdrawBalance(float newBalance) {
        balance = balance - newBalance;
    }
}
