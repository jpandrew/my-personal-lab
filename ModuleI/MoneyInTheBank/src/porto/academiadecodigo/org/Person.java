package porto.academiadecodigo.org;

public class Person {

    private String name;
    private Wallet wallet;
    private Savings savings;

    public Person(String name, Wallet wallet, Savings savings) {

        this.name = name;
        this.wallet = wallet;
        this.savings = savings;


    }

    public void toEarn(float value) {
        wallet.depositBalance(value);
        System.out.println("Added " + value + " bucks to your account");
        System.out.println(wallet.checkBalance());
    }

    public void toSpend(float value) {
        if (wallet.operationPossible(value)) {
            wallet.withdrawBalance(value);
            System.out.println("You spent " + value + " bucks. " + wallet.checkBalance());


        } else {
            System.out.println("You don't have money!" + "\n" + wallet.checkBalance());
        }
    }

    public void toDepositSavings(float value) {
        if (wallet.operationPossible(value)) {
            wallet.withdrawBalance(value);
            savings.depositBalance(value);
            System.out.println("You transferred " + value + " to your savings." + "\n" + wallet.checkBalance() + "\n" + savings.checkBalance());
        } else {
            System.out.println("You don't have enough money in your wallet to proceed with this operation!");
        }
    }

    public void toWithdrawSavings(float value) {

        if (savings.operationPossible(value)) {
            wallet.depositBalance(value);
            savings.withdrawBalance(value);
            System.out.println("You transferred " + value + " to your wallet." + "\n" + wallet.checkBalance() + "\n" + savings.checkBalance());
        } else {
            System.out.println("You don't have enough money in your savings to proceed with this operation!");
        }
    }
}

