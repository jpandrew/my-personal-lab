class closestNeighbours {
	public static void main(String[] args){
		
		int[] myArray = {0, 113, 112, 6, 2, 33, 115, 111};
		
		int[] result = findClosest(myArray);

		for(int element: result){  
			System.out.println(element);
		}
	}
	private static int[]  findClosest(int[] myArray) {

			int[] resultArray = new int[2];
			int arrayLength = myArray.length - 1;
			int key = Math.abs(myArray[0] - myArray[1]);
		
			for(int i = 1; i < arrayLength; i++){
				
				int differencePair = Math.abs(myArray[i] - myArray[i+1]);

					if(key > differencePair){

						key = differencePair;
						resultArray[0] = myArray[i];
						resultArray[1] = myArray[i+1];
						System.out.println("The key value is " + key);
					}
			}
			return resultArray;
	}
} 
