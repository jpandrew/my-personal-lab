class missing_char {
	public static void main(String[] args) {

  	missingChar("Robocop", 0);

  //receive command line argument if available
  		if(args.length>=2){
   			 missingChar(args[0], Integer.parseInt(args[1]));
 		 }		
	}	

	private static void missingChar(String str, int n) {

		if(str.length() > 0){
			String start = str.substring(0,n);
			String end = str.substring(n+1);
			String result = start + end;
			System.out.println(result);	
		}	
	}
} 
