package org.academiadecodigo.floppybirds;

import org.academiadecodigo.floppybirds.fighters.*;

import java.util.Arrays;

public class Player {

    private String name;
    private Fighter[] fighters;

    public Player(String name) {
        this.name = name;
        fighters = createFighters();
    }

    public void attack(Player enemy) {
        Fighter myFighter = pickAliveFighter(fighters);
        Fighter enemyFighter = pickAliveFighter(enemy.fighters);

        if (Math.random() > 0.5) {
            myFighter.hit(enemyFighter);
        } else {
            myFighter.cast(enemyFighter);
        }

        System.out.println(name + " plays " + myFighter +
                " attacking an enemy champion and leaving him " +
                enemyFighter);
    }

    public boolean hasLost() {
        boolean lost = true;
        for (Fighter fighter : fighters) {
            if (!fighter.isDead()) {
                lost = false;
            }
        }
        return lost;
    }

    private Fighter[] createFighters() {
        String[] names = {"David", "Telmo", "Daniel", "Andreia", "Paulo", "Raquerita", "Jonathan", "Guilherme", "Victor", "Vinagreiro", "Gessica", "Ricardo", "Nuno"};
        Fighter[] toReturn = new Fighter[5];

        for (int i = 0; i < toReturn.length; i++) {
            FighterType type = FighterType.getRandom();
            String name = names[(int) (Math.random() * names.length)];
            switch (type) {
                case TROLL:
                    toReturn[i] = new Troll(name);
                    break;
                case WIZARD:
                    toReturn[i] = new Wizard(name);
                    break;
                case GLADIATOR:
                    toReturn[i] = new Gladiator(name);
                    break;
            }
        }
        return toReturn;
    }

    private Fighter pickAliveFighter(Fighter[] array){
        Fighter fighter = null;
        while (fighter == null || fighter.isDead()) {
            fighter = array[(int) (Math.random() * array.length)];
        }
        return fighter;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", fighters=" + Arrays.toString(fighters) +
                '}';
    }
}
