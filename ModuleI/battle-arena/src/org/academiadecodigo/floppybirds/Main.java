package org.academiadecodigo.floppybirds;

public class Main {
    public static void main(String[] args) {
        Player player1 = new Player("Rolo");
        Player player2 = new Player("NotRolo");

        Arena deathRing = new Arena(player1, player2);
        deathRing.battle();
    }
}
