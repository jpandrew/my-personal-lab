package org.academiadecodigo.floppybirds.fighters;

public enum FighterType {
    TROLL(8, 2, 300),
    WIZARD(6, 20, 70),
    GLADIATOR(30, 5, 50);

    public final int ATTACK_DAMAGE;
    public final int SPELL_DAMAGE;
    public final int INITIAL_HEALTH;

    FighterType(int attackDamage, int spellDamage, int health){
        ATTACK_DAMAGE =  attackDamage;
        SPELL_DAMAGE = spellDamage;
        INITIAL_HEALTH = health;
    }

    public static FighterType getRandom(){
        return values()[(int)(Math.random() * values().length)];
    }
}
