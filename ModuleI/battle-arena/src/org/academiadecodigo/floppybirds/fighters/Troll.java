package org.academiadecodigo.floppybirds.fighters;

public class Troll extends Fighter {

    private static final int ATTACK_POSSIBILITY = 50;

    public Troll(String name) {
        super(name, FighterType.TROLL);
}

    @Override
    public void hit(Fighter enemy) {
        if ((Math.random() * 100) < ATTACK_POSSIBILITY) {
            super.hit(enemy);
        }
    }
}
