package org.academiadecodigo.floppybirds.fighters;

public class Gladiator extends  Fighter{

    private static final int CRITICAL_CHANCE = 10;
    private static final int CRITICAL_MULTIPLIER = 2;

    public Gladiator(String name) {
        super(name, FighterType.GLADIATOR);
    }

    @Override
    public void hit(Fighter enemy){
        if((Math.random() * 100) < CRITICAL_CHANCE){
            enemy.suffer(FighterType.GLADIATOR.ATTACK_DAMAGE * CRITICAL_MULTIPLIER);
        } else {
            super.hit(enemy);
        }
    }
}
