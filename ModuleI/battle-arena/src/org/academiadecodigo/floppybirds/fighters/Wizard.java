package org.academiadecodigo.floppybirds.fighters;

public class Wizard extends Fighter {

    private static final int SHIELD_PROBABILITY = 15;

    private boolean shield;

    public Wizard(String name) {
        super(name, FighterType.WIZARD);
        shield = false;
    }

    @Override
    public void cast(Fighter enemy) {
       super.cast(enemy);
       if((Math.random() * 100 ) < SHIELD_PROBABILITY){
            shield = true;
       }
    }

    @Override
    public void suffer(int dmg) {
        if(shield){
            shield = false;
            return;
        }
        super.suffer(dmg);
    }
}
