package org.academiadecodigo.floppybirds;

public class Arena {

    private Player player1;
    private Player player2;

    public Arena(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void battle(){
        while (!(player1.hasLost() || player2.hasLost())){
            player1.attack(player2);

            if(!player2.hasLost()){
                player2.attack(player1);
            }
        }

        System.out.println("============" +
                (player1.hasLost() ? " player2 " : "player1") +
                " has won!" +
                "============");

        System.out.println("=== END STATE ===");
        System.out.println("PLAYER 1");
        System.out.println(player1);
        System.out.println("\nPLAYER 2");
        System.out.println(player2);
    }

}
