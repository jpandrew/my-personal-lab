import java.util.*;

public class UniqueWord implements  Iterable<String>{

    private Set<String> set;

    public UniqueWord(String string) {
        this.set = new LinkedHashSet<>();
        splitString(string);

    }

    public void splitString(String string) {
        String[] splited = string.split("\\s+");
        set.addAll(Arrays.asList(splited));
    }


    @Override
    public Iterator<String> iterator() {
        return set.iterator();
    }
}
