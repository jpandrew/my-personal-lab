package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Direction;
import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {

    /**
     * The position of the car on the grid
     */
    private Position pos;
    private String brand;
    protected boolean crashed;
    private static final int SAME_DIRECTION_CHANCE = 70;


    public Car(Position pos) {
        this.pos = pos;
    }


    public void chooseDirection() {
        if (!crashed) {
            Direction[] currentDirection = Direction.values();
            int index = (int) Math.floor(Math.random() * currentDirection.length);
            int nextDirection = (int) Math.floor(Math.random()*101);
            if(nextDirection < SAME_DIRECTION_CHANCE) {
                switch (currentDirection[index]) {
                    case UP:
                        moveUp();
                        break;
                    case DOWN:
                        moveDown();
                        break;
                    case LEFT:
                        moveLeft();
                        break;
                    case RIGHT:
                        moveRight();
                        break;
                }
            }
/*            else {
                Direction previousDirection = currentDirection;
            }*/
        }
    }

    public void moveUp() {
        if (!pos.isOutOfBounds(Direction.UP)) {
            pos.updatePosition(Direction.UP);
        }
    }

    public void moveDown() {
        if (!pos.isOutOfBounds(Direction.DOWN)) {
            pos.updatePosition(Direction.DOWN);
        }
    }

    public void moveLeft() {
        if (!pos.isOutOfBounds(Direction.LEFT)) {
            pos.updatePosition(Direction.LEFT);
        }
    }

    public void moveRight() {
        if (!pos.isOutOfBounds(Direction.RIGHT)) {
            pos.updatePosition(Direction.RIGHT);
        }
    }


    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashed(boolean isCrashed) {
        crashed = isCrashed;
    }

    public Position getPos() {
        return pos;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "Car{" +
                "pos=" + pos +
                ", brand='" + brand + '\'' +
                '}';
    }
}
