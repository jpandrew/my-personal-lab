package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;


public class CarFactory {

    public static Car getNewCar() {
        int row = (int) Math.floor(Math.random() * Field.getHeight());
        int col = (int) Math.floor(Math.random() * Field.getWidth());
        Position newPos = new Position(col, row);
        if ((int) Math.floor(Math.random() * 101) < 50) {
            return new Smart(newPos);
        }
        return new Tesla(newPos);
    }

}

