package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Tesla extends Car{


    private String brand;
    private int speed = 2;
    private static final int SAME_DIRECTION_CHANCE = 90;

    public Tesla(Position pos){
        super(pos);
        this.brand = "T";

    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public void setCrashed(boolean isCrashed) {
        crashed = isCrashed;
        setBrand("C");
    }
}
