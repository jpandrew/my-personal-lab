package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Smart extends Car {

    private String brand;
    private static final int SAME_DIRECTION_CHANCE = 85;
    private int moveCounter = 0;

    public Smart(Position pos) {
        super(pos);
        this.brand = "S";
        moveCounter = 0;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setCrashed(boolean isCrashed) {
        crashed = isCrashed;
        setBrand("C");
    }

    @Override
    public void chooseDirection() {
        if (moveCounter == 3) {
            super.chooseDirection();
            moveCounter = 0;
            return;
        }
        moveCounter++;
    }

    @Override
    public String toString() {
        return brand;
    }
}
