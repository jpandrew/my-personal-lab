package org.academiadecodigo.bootcamp.containers;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {

        LinkedList<Integer> range = new LinkedList();

        range.add(1);
        range.add(2);
        range.add(5);
        range.remove(2);

        for(Integer i: range){
            System.out.println(i);
        }
        Iterator<Integer> it = range.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }

    }

}