package org.academiadecodigo.bootcamp.containers;


import java.util.Iterator;

public class LinkedList<T> implements Iterable<T> {

    private Node head;
    private int length = 0;

    public LinkedList() {
        this.head = new Node(null);
    }

    public int size() {
        return length;
    }


    @Override
    public Iterator<T> iterator() {
        return new MyIterator();
    }

    /**
     * Adds an element to the end of the list
     * @param data the element to add
     */
    public void add(T data)  {

        Node node = new Node(data);
        Node iterator = head;
        while (iterator.getNext() != null){
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;


    }

    /**
     * Obtains an element by index
     * @param index the index of the element
     * @return the element
     */
    public T get(int index) {


            int i = 0;
            Iterator<T> iterator = iterator();

            while (iterator.hasNext()){
                if(i == index){
                    return iterator.next();
                }
                iterator.next();
                i++;
            }
            return null;
    }

    /**
     * Returns the index of the element in the list
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(T data) {

        Iterator<T> iterator = iterator();
        int i = 0;

          while (iterator.hasNext()){
            if(iterator.next().equals(data)){
                return i;
            }
            iterator.next();
            i++;
        }
        return -1;
    }

    /**
     * Removes an element from the list
     * @param data the element to remove
     * @return true if element was removed
     */
    public boolean remove(T data) {
        Iterator iterator = iterator();

            while (iterator.hasNext()) {
                if (iterator.equals(data)) {
                    iterator.remove();
                    length--;
                    return true;
                }
                iterator.next();
            }
        return false;
    }




    private class MyIterator implements Iterator<T>{

        private Node iterator = head;


        @Override
        public boolean hasNext() {
            return iterator.getNext() != null;
        }

        @Override
        public T next() {
            iterator = iterator.getNext();
            return iterator.getData();
        }

        @Override
        public void remove() {
        Node next = iterator.getNext();
        iterator.setNext(next.getNext());
        }

        public Node getNode(){
            return iterator;
        }

    }


    private class Node {

        private T data;
        private Node next;

        public Node(T data) {
            this.data = data;
            next = null;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }
}
