import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class WordReader implements Iterable<String> {

    private String result = "";
    private ArrayList<String> arrayList;
    private FileReader fileReader;
    private BufferedReader bufferedReader;


    public WordReader(String file) {
        this.arrayList = new ArrayList<>();
        copy(file);
    }

    public void splitString(String result) {
        String[] splited = result.split("\\W+");
        arrayList.addAll(Arrays.asList(splited));
    }

    public void copy(String file) {
        try {
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result += line + "\n";
                splitString(result);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            cleanUp();
        }
    }

    public void cleanUp() {
        try {
            if (fileReader != null) {
                fileReader.close();
            }
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        return "WordReader{" +
                "arrayList=" + arrayList +
                '}';
    }

    @Override
    public Iterator<String> iterator() {
        return arrayList.iterator();
    }

}
