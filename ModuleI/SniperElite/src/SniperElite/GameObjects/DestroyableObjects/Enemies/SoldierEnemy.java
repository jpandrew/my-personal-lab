package SniperElite.GameObjects.DestroyableObjects.Enemies;

import SniperElite.GameObjects.DestroyableObjects.Destroyable;
import SniperElite.GameObjects.DestroyableObjects.Enemies.Enemy;

public class SoldierEnemy extends Enemy{

    public SoldierEnemy(int health) {
        super(health);
    }

    @Override
    public boolean isDestroyed() {
        return super.isDestroyed;
    }

    public void hit(int damage){
        super.hit(damage);
        if(health > 0 && !(damage==0)) {
            System.out.println("Soldier enemy has been hit for " + damage + " . Health is now " + health + ".");
        }
    }

    @Override
    public String toString() {
        return "SoldierEnemy{" +
                "health=" + health +
                '}';
    }
}
