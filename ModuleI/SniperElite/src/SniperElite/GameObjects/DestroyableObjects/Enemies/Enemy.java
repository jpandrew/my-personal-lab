package SniperElite.GameObjects.DestroyableObjects.Enemies;

import SniperElite.GameObjects.DestroyableObjects.Destroyable;
import SniperElite.GameObjects.GameObject;

public abstract class Enemy extends GameObject implements Destroyable {

    protected int health;
    public boolean isDestroyed;

    public Enemy(int health) {
        this.health = health;
        isDestroyed = false;
    }

    public int getHealth() {
        return health;
    }

    public void hit(int damage) {
            health = health - damage;
            if(damage == 0) {
                System.out.println("Missed shot");
            }
            if (health <= 0) {
                isDestroyed = true;
                health = 0;
                System.out.println("Hit confirmed. Enemy down!");
            }
    }

    @Override
    public String toString() {
        return "Enemy{" +
                "health=" + health +
                ", isDead=" + isDestroyed +
                '}';
    }
}
