package SniperElite.GameObjects.DestroyableObjects.Enemies;

import SniperElite.GameObjects.DestroyableObjects.Destroyable;

public class ArmouredEnemy extends Enemy {

    private int armour;

    public ArmouredEnemy(int health, int armour) {
        super(health);
        this.armour = armour;
    }

    @Override
    public boolean isDestroyed() {
        return super.isDestroyed;
    }

    @Override
    public void hit(int damage) {
        int dif = 0;
        if (damage > armour && !(damage == 0)) {
            dif = damage - armour;
            armour = 0;
            super.hit(dif);
            if(health > 0) {
                System.out.println("Armoured Enemy has been hit for " + damage + ". Health is now: " + health + ".");
            }
        } else if (armour > 0 && !(damage == 0)) {
            armour = armour - damage;
            System.out.println("Armoured Enemy has been hit for " + damage + ". Armour is now: " + armour + ".");
        }
    }

    @Override
    public String toString() {
        return "ArmouredEnemy{" +
                "armour=" + armour +
                ", health=" + health +
                '}';
    }
}
