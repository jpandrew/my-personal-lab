package SniperElite.GameObjects.DestroyableObjects;

public enum BarrelType {

    PLASTIC(100),
    WOOD(50),
    METAL(200);

    private final int maxDamage;

    BarrelType(int maxDamage) {
        this.maxDamage = maxDamage;
    }


    public int getMaxDamage(){
        return maxDamage;
    }

    public static BarrelType chooseBarrel() {
        int index = (int) Math.floor(Math.random() * values().length);
        return values()[index];
    }
}
