package SniperElite.GameObjects.DestroyableObjects;

public interface Destroyable {

    public boolean isDestroyed();
    public void hit(int damage);
}
