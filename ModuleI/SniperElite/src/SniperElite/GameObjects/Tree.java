package SniperElite.GameObjects;

import SniperElite.GameObjects.GameObject;

public class Tree extends GameObject {

    private String name;

    public Tree() {
        this.name = "Tree";
    }

    @Override
    public String toString() {
        return name;
    }
}
