package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Integer> queue;
    private int foodToEat;
    private int eatenFood;

    /**
     * @param queue      the blocking queue to consume elements from
     * @param elementNum the number of elements to consume
     */
    public Consumer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.foodToEat = elementNum;
    }

    @Override
    public void run() {
        while (eatenFood < foodToEat) {
            synchronized (queue) {
                queue.poll();
                eatenFood++;
                System.out.println(Thread.currentThread().getName() + " CONSUMED SOME FRESH FOOD");
                System.out.println("There is now: " + queue.getSize() + " left to be eaten!");
                if (queue.getSize() == 0) {
                    System.out.println("There's nothing left to eat!!!");
                }
            }
            try {
                Thread.sleep((int) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + " is completely full. Going home.");
    }
}

