package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Integer> queue;
    private final int foodToMake;
    private int foodMade;

    /**
     * @param queue      the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.foodToMake = elementNum;
    }

    @Override
    public void run() {
        while (foodMade < foodToMake) {
            synchronized (queue) {
                queue.offer(1);
                foodMade++;
                System.out.println(Thread.currentThread().getName() + " just made some fresh panados");
                System.out.println("There is now: " + queue.getSize() + " left to be eaten!");
                if (queue.getSize() == queue.getLimit()) {
                    System.out.println(Thread.currentThread().getName() + ": Too much food already");
                }
            }
            try {
                Thread.sleep((int) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + " is done for today. Going home.");
    }
}
