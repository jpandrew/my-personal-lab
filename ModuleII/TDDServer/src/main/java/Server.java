import ServerMessages.Messages;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private ServerSocket serverSocket;
    private Socket clientSocket;

    public Server(){

    }

    public void init(){
        try {
            serverSocket = new ServerSocket(42069);
            System.out.println(Messages.SERVER_PORT_CONNECTED + serverSocket.getLocalPort());
        } catch (IOException e) {
            e.printStackTrace();
        }
        connect();
    }

    public void connect(){
        try {
            clientSocket = serverSocket.accept();
            System.out.println(Messages.CLIENT_CONNECTION_FOUND + clientSocket.getLocalAddress());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void dispatch(){

    }

    public void broadCastMessage(){}


}
