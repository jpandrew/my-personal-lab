package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TCPServer {


    public static final int PORT_NUMBER = 42069;
    private ServerSocket serverSocket;

    private ExecutorService cachedPool;
    private static int counter;

    private final List<ServerWorker> workerList;


    public static void main(String[] args) {
        TCPServer tcpServer = new TCPServer();
        tcpServer.listen();
    }
    public TCPServer(){
        cachedPool = Executors.newCachedThreadPool();
        workerList = new LinkedList<>();
    }

    private void listen() {
        try {
            serverSocket = new ServerSocket(PORT_NUMBER);

            while (true) {
                System.out.println("Waiting for connection...");
                Socket clientSocket = serverSocket.accept();
                dispatch(clientSocket);
            }


        } catch (NumberFormatException ex) {

            System.err.println("Error: Invalid port!");

        } catch (SocketException ex) {

            System.err.println("Error: Could not accept incoming packets: " + ex.getMessage());

        } catch (IOException ex) {

            System.err.println("Error: Network failure: " + ex.getMessage());
        } finally {
            try {
                cleanUp();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public String listClients() {
        StringBuilder list = new StringBuilder("Connected Clients:\n");
        synchronized (workerList) {
            for (ServerWorker worker : workerList) {
                list.append(worker.getUserName()).append("\n");
            }
        }
        return list.toString();
    }


    public void dispatch(Socket clientSocket) {
        ServerWorker worker = new ServerWorker(clientSocket, this);
        counter++;
        worker.setUserName("User" + counter);
        System.out.println(worker.getUserName() + " connected to server: " + worker.getAddress());
        cachedPool.submit(worker);
        workerList.add(worker);
        for (ServerWorker w : workerList) {
            w.sendToClient(worker.getUserName() + " has joined the server!");
        }
    }


    public synchronized void broadcastMessage(ServerWorker worker, String clientMessage) {
        for (ServerWorker w : workerList) {
            if (!w.getUserName().equals(worker.getUserName())) {
                w.sendToClient(w.getUserName() + " says: " + clientMessage);
            }
        }
    }


    public void whisperUser(String clientMessage, ServerWorker worker) {
        String[] whisper = clientMessage.split(" ", 3);
        if(whisper.length < 3){
            worker.sendToClient(ServerCommands.whisperMessageNotFound());
            return;
        }

        for (ServerWorker w : workerList) {
            if (whisper[1].equals(w.getUserName())) {
                w.sendToClient("<Whisper from " + worker.getUserName() + "> " + whisper[2]);
                return;
            }
        }
        worker.sendToClient(ServerCommands.UserNotFound());
    }

    public ServerWorker getClientByName(String name) {
        synchronized (workerList) {
            for (ServerWorker w : workerList) {
                if (w.getUserName().equals(name)) {
                    return w;
                }
            }
        }
        return null;
    }


    public void CommandNotFound(ServerWorker worker){
        worker.sendToClient(ServerCommands.CommandNotFound());
    }

    public void removeUserFromList(ServerWorker worker) {
        workerList.remove(worker);
    }

    public void cleanUp() throws IOException {
        serverSocket.close();

    }


}
