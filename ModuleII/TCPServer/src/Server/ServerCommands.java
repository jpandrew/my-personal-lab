package Server;

import java.io.PrintWriter;
import java.util.Arrays;

public class ServerCommands {

    public static void changeName(String clientMessage, ServerWorker worker) {
        String[] changeName = clientMessage.split(" ", 2);
        worker.setUserName(changeName[1]);
    }

    public static String getCommandList() {
        return "Commands available are:" +
                "\n/name <newName> : Allows your username to be changed to the new name." +
                "\n/whisper <UserName> : Allows you to send a private message to the target username."+
                "\n/quit : Ends your session.";
    }

    public static String CommandNotFound(){
        return "Command not found. Please check your writing.";
    }

    public static String UserNotFound(){
        return "User could not be found or is offline. Please check the name again.";
    }

    public static String whisperMessageNotFound(){
        return "Please write your message! Empty messages are not allowed.";
    }

}
