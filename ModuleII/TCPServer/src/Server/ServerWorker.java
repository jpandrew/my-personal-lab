package Server;

import Server.Commands.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class ServerWorker implements Runnable {

    private final Socket clientSocket;
    private final TCPServer tcpServer;

    private BufferedReader in;
    private PrintWriter out;
    private String name;

    public ServerWorker(Socket clientSocket, TCPServer tcpServer) {
        this.tcpServer = tcpServer;
        this.clientSocket = clientSocket;
        initStreams();
    }

    public String getAddress() {
        if (clientSocket == null) {
            return null;
        }
        return clientSocket.getInetAddress().getHostAddress() + ":" + clientSocket.getLocalPort();
    }


    @Override
    public void run() {

        sendToClient(Messages.WELCOME);

        try {
            while (!clientSocket.isClosed()) {
                listen(in);
                /*f ((clientMessage = in.readLine()) != null) {
                        if (clientMessage.contains("/name")) {
                            ServerCommands.changeName(clientMessage, this);
                            out.println("Name changed to: " + name);

                        } else if (clientMessage.contains("/help")) {
                            out.print(ServerCommands.getCommandList());
                        } else if (clientMessage.contains("/quit")) {
                            out.println("Your session will now end");
                            cleanUp();
                            tcpServer.removeUserFromList(this);
                        } else if (clientMessage.contains("/whisper")) {
                            tcpServer.whisperUser(clientMessage, this);*/
                      /* Attempt to stay on whisper mode while user does not say /say to talk in
                      public chat. Might require complete revamp of the whisperUser method.
                      while (true){
                                if((clientMessage = in.readLine()) != null){
                                    if(clientMessage.contains("/say")){
                                        break;
                                    }
                                    tcpServer.whisperUser(clientMessage,this);
                                }
                            }*/
                     /*   } else if (clientMessage.contains("/")) {
                            tcpServer.CommandNotFound(this);
                        } else {
                            tcpServer.broadcastMessage(this, clientMessage);
                        }*/
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            cleanUp();
        }
    }

    private void initStreams() {
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(clientSocket.getOutputStream(), true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listen(BufferedReader in) throws IOException {
        String message = in.readLine();
        Command.getFromString(message).getHandler().handle(tcpServer, this, message);
    }


    public void sendToClient(String clientMessage) {
        out.println(clientMessage);
    }

    public String getUserName() {
        return name;
    }

    public void setUserName(String name) {
        this.name = name;
    }


    public void cleanUp() {
        try {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
            if (clientSocket != null) {
                clientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
