package Server.Commands;

import Server.Messages;
import Server.ServerWorker;
import Server.TCPServer;

public class QuitHandler implements CommandHandler{

    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {
        server.removeUserFromList(sender);
        server.broadcastMessage(sender,sender.getUserName() + " " + Messages.LEAVE);
        sender.cleanUp();
    }
}

