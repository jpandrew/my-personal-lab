package Server.Commands;

import Server.ServerWorker;
import Server.TCPServer;

public interface CommandHandler {
    void handle(TCPServer server, ServerWorker sender, String message);
}

