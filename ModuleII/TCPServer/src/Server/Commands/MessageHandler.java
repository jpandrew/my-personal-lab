package Server.Commands;

import Client.TCPClient;
import Server.ServerWorker;
import Server.TCPServer;

public class MessageHandler implements CommandHandler{

    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {

        if (!isValid(message)) {
            return;
        }
        server.broadcastMessage(sender,sender.getUserName() + ": " + message);
    }

    private boolean isValid(String message) {
        return !message.trim().isEmpty();
    }
}

