package Server.Commands;

import Server.Messages;
import Server.ServerWorker;
import Server.TCPServer;

public class HelpHandler implements CommandHandler{
    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {
        sender.sendToClient(Messages.HELP);
    }
}
