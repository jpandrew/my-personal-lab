package Server.Commands;

import Server.Messages;
import Server.ServerWorker;
import Server.TCPServer;

public class InvalidHandler implements CommandHandler{

    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {
        sender.sendToClient(Messages.ERROR + ": " + (message.startsWith(Messages.COMMAND_IDENTIFIER) ? Messages.INVALID_COMMAND : message));
    }
}

