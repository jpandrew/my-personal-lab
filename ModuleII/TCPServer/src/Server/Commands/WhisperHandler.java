package Server.Commands;

import Server.Messages;
import Server.ServerWorker;
import Server.TCPServer;

public class WhisperHandler implements CommandHandler{


    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {

        if (!isValid(message)) {
            Command.INVALID.getHandler().handle(server, sender, Messages.WHISPER_USAGE);
            return;
        }

        ServerWorker receiver = server.getClientByName(message.split(" ")[1]);

        if (receiver == null) {
            Command.INVALID.getHandler().handle(server, sender, Messages.INVALID_USERNAME);
            return;
        }
        message = removeWords(message, 2);
        receiver.sendToClient(sender.getUserName() + Messages.WHISPER + ": " + message);
    }

    private boolean isValid(String message) {
        return message.split(" ").length > 2;
    }

    private String removeWords(String phrase, int wordsToRemove) {
        while (wordsToRemove > 0) {
            phrase = phrase.substring(phrase.indexOf(' ') + 1);
            wordsToRemove--;
        }

        return phrase;
    }
}

