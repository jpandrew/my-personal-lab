package Server.Commands;

import Server.Messages;
import Server.ServerWorker;
import Server.TCPServer;

public class NameHandler implements CommandHandler{

    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {

        if (!isValid(message)) {
            Command.INVALID.getHandler().handle(server, sender, Messages.NAME_USAGE);
            return;
        }

        String newName = message.split(" ")[1];

        if (server.getClientByName(newName) != null) {
            Command.INVALID.getHandler().handle(server, sender, Messages.NAME_IN_USE);
            return;
        }

        server.broadcastMessage(sender,sender.getUserName() + " " + Messages.RENAME + " " + newName);
        sender.setUserName(newName);
    }

    private boolean isValid(String message) {
        return message.split(" ").length == 2;
    }
}

