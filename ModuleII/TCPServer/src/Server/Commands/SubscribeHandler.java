package Server.Commands;

import Server.Messages;
import Server.ServerWorker;
import Server.TCPServer;

public class SubscribeHandler implements CommandHandler {
    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {
        if(sender.isSubscribed()){
            sender.sendToClient(Messages.ALREADY_SUBSCRIBED);
            return;
        }
        sender.setSubscribed(true);
        sender.sendToClient(Messages.SUBSCRIBE);

    }
}
