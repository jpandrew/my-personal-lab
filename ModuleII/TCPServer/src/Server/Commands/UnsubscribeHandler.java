package Server.Commands;

import Server.Messages;
import Server.ServerWorker;
import Server.TCPServer;

public class UnsubscribeHandler implements CommandHandler {
    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {
        if(!sender.isSubscribed()){
            sender.sendToClient(Messages.ALREADY_UNSUBSCRIBED);
            return;
        }
        sender.setSubscribed(false);
        sender.sendToClient(Messages.UNSUBSCRIBE);
    }
}
