package Client;

import Server.ServerWorker;
import Server.TCPServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class TCPClient {

    private static final InetAddress HOST = InetAddress.getLoopbackAddress();
    private Socket socket;
    private TCPServer tcpServer;


    public static void main(String[] args) {

        try {
            TCPClient client = new TCPClient();
            client.start();

        } catch (IOException e) {
            System.err.println("Error connecting to server: " + e.getMessage());

        } catch (NumberFormatException e) {
            System.err.println("Error port must be a valid number: " + args[1]);
        }
    }



    public TCPClient() throws IOException {
        socket = new Socket(HOST, TCPServer.PORT_NUMBER);
    }



    public void start() {
        Thread keyboard = new Thread(new KeyboardHandler(socket));
        keyboard.start();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            while (!socket.isClosed()) {
                waitMessage(reader);
            }

        } catch (IOException e) {
            System.err.println("Error handling socket connection: " + e.getMessage());
        }
    }

    private void waitMessage(BufferedReader reader) throws IOException {
        String message = reader.readLine();

        if (message == null) {
            System.out.println("Connection closed from server side");
            System.exit(0);
        }

        System.out.println(message);
    }
}




