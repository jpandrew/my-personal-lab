import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPServer {
    private static final int PORT_NUMBER = 42069;
    private static final String CHARSET = "UTF8";

    public static void main(String args[]) {

        try {
            // create a UDPServer object
            UDPServer udpServer = new UDPServer();


            // start the udp server
            udpServer.start();
        } catch (NumberFormatException ex) {

            System.err.println("Error: Invalid port!");

        } catch (SocketException ex) {

            System.err.println("Error: Could not accept incoming packets: " + ex.getMessage());

        } catch (IOException ex) {

            System.err.println("Error: Network failure: " + ex.getMessage());
        }
    }

    private void start() throws IOException {

        DatagramSocket serverSocket = new DatagramSocket(PORT_NUMBER);

        byte[] receiveData = new byte[1024];

        while (serverSocket.isBound()) {

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

            System.out.println("Waiting for datagram packet");
            serverSocket.receive(receivePacket);

            String message = new String(receivePacket.getData(), 0, receivePacket.getLength(), CHARSET);

            InetAddress address = receivePacket.getAddress();
            int port = receivePacket.getPort();
            System.out.println("From: " + address.getHostName() + ":" + port);
            System.out.println("Message: " + message);

            String upperCase = message.toUpperCase();
            byte[] sendData = upperCase.getBytes(CHARSET);

            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, address, port);
            serverSocket.send(sendPacket);

        }

    }
}

