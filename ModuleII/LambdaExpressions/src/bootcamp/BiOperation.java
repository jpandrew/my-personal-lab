package bootcamp;

public interface BiOperation<T> {

    T biOperation(T data, T moreData);
}
