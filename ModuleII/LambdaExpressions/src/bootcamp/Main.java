package bootcamp;

public class Main {


    public static void main(String[] args) {
        Machine machine = new Machine();

        int i1 = 5;
        machine.makeMonoOp(i1);
        System.out.println(machine.integerMonoOperation.singleOperation(10));
        System.out.println(machine.integerBiOperation.biOperation(2,2));
        System.out.println(machine.stringBiOperation.biOperation("this string ", "and this one"));
    }
}
