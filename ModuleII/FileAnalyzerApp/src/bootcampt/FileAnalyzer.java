package bootcampt;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileAnalyzer {

    Path filePath = Paths.get("resources/text.txt");
    Path anotherFilePath = Paths.get("resources/anotherText.txt");

    public void doEverything(){
        try {
            System.out.println("---------------------------------------------");
            System.out.println("Number of words in document: " + countWords());
            System.out.println("---------------------------------------------");
            Thread.sleep(1000);
            System.out.println("First word occurrence with desired length: ");
            System.out.println(firstLongestWord(3));
            System.out.println("---------------------------------------------");
            Thread.sleep(1000);
            System.out.println("List of all words with desired length: ");
            printStream(allLongestWords(3));
            System.out.println("---------------------------------------------");
            Thread.sleep(1000);
            System.out.println("List of common words between files: ");
            printStream(findCommonWords());
            System.out.println("---------------------------------------------");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Stream<String> readFileAndBreakWords(Path filePath) throws IOException {
        Stream<String> lines = Files.lines(filePath, Charset.defaultCharset());
        return lines.flatMap(line -> Arrays.stream(line.split("[^a-zA-Z0-9']+")));
    }

    private Stream<String> readFileAndBreakWords() throws IOException {
       return readFileAndBreakWords(filePath);
    }

    private long countWords() throws IOException {
        return readFileAndBreakWords()
                .count();
    }

    private String firstLongestWord(int n) throws IOException {
        return readFileAndBreakWords()
                .filter(word -> word.length() == n)
                .findFirst()
                //.ifPresent(System.out::println) void return
                .orElse("No words with that length");

    }

    private Stream<String> allLongestWords(int n) throws IOException {
        return readFileAndBreakWords()
                .distinct()
                //.sorted((s1,s2) -> s2.length() - s1.length())
                //.sorted(Comparator.comparing(String::length,Comparator.reverseOrder()))
                .sorted(Comparator.comparingInt(String::length).reversed())
                .limit(n);
    }


    private Stream<String> findCommonWords() throws IOException {
        Set<String> uniqueWords =  readFileAndBreakWords(anotherFilePath).collect(Collectors.toSet());

        return readFileAndBreakWords()
                .filter(uniqueWords::contains);

    }

    private void printStream(Stream<String> stream){
        stream.forEach(System.out::println);
    }



}