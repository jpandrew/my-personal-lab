package Philosophers;

public class Philosopher implements Runnable {

    private final ChopstickVault<Chopstick> vault;
    private boolean isThinking;
    private boolean hasChopsticks;
    private final int riceLimit;
    private int eatenRice;
    private final Chopstick[] chopsticks = new Chopstick[2];

    public Philosopher(ChopstickVault<Chopstick> vault, int riceLimit) {
        this.isThinking = Math.floor(Math.random() * 2) == 0;
        hasChopsticks = false;
        this.riceLimit = riceLimit;
        this.vault = vault;
    }


    @Override
    public void run() {
        while (eatenRice < riceLimit) {
            while (!hasChopsticks) {
                synchronized (vault) {
                    if (chopsticks[0] == null) {
                        chopsticks[0] = vault.remove();
                        System.out.println(Thread.currentThread().getName() + ":  picked up a chopstick." + " There are now: " + vault.getSize() + " chopsticks left in the vault");
                    }
                    if (Math.floor(Math.random() * 2) == 0) {
                        isThinking = true;
                    }
                    if (isThinking) {
                        System.out.println(Thread.currentThread().getName() + " started to contemplate the abyss");
                        for (int i = 0; i < chopsticks.length; i++) {
                            if (chopsticks[i] != null) {
                                vault.add(chopsticks[i]);
                                chopsticks[i] = null;
                                System.out.println(Thread.currentThread().getName() + " dropped one chopstick");
                            }
                        }
                        hasChopsticks = false;
                    }
                }
                if (Math.floor(Math.random() * 2) == 0) {
                    isThinking = false;
                }
                synchronized (vault) {
                    if (chopsticks[1] == null) {
                        chopsticks[1] = vault.remove();
                        System.out.println(Thread.currentThread().getName() + ":  picked up a chopstick." + " There are now: " + vault.getSize() + " chopsticks left in the vault");
                    }
                }
                if (isThinking) {
                    System.out.println(Thread.currentThread().getName() + " started to contemplate the abyss");
                    for (int i = 0; i < chopsticks.length; i++) {
                        if (chopsticks[i] != null) {
                            vault.add(chopsticks[i]);
                            chopsticks[i] = null;
                            System.out.println(Thread.currentThread().getName() + " dropped one chopstick");
                        }
                    }
                    hasChopsticks = false;
                }
                if (chopsticks[0] != null && chopsticks[1] != null) {
                    hasChopsticks = true;
                }
                if (Math.floor(Math.random() * 2) == 0) {
                    isThinking = false;
                }
            }
            while (hasChopsticks) {
                eatenRice++;
                System.out.println(Thread.currentThread().getName() + " had some rice." + " Only " + (riceLimit - eatenRice) + " spoons left.");
                if (eatenRice == riceLimit) {
                    for (int i = 0; i < chopsticks.length; i++) {
                        if (chopsticks[i] != null) {
                            vault.add(chopsticks[i]);
                            chopsticks[i] = null;
                        }
                    }
                    System.out.println(Thread.currentThread().getName() + " has dropped both chopsticks because he finished eating.");
                    return;
                    }
                    if (Math.floor(Math.random() * 2) == 0) {
                        isThinking = true;
                    }
                    if (isThinking) {
                        System.out.println(Thread.currentThread().getName() + " started to contemplate the abyss");
                        for (int i = 0; i < chopsticks.length; i++) {
                            if (chopsticks[i] != null) {
                                vault.add(chopsticks[i]);
                                chopsticks[i] = null;
                                System.out.println(Thread.currentThread().getName() + " dropped one chopstick");
                            }
                        }
                        hasChopsticks = false;
                    }
                    if (Math.floor(Math.random() * 2) == 0) {
                        isThinking = false;
                    }
                }
            }
            run();
        }
    }
