package Philosophers;

import java.util.LinkedList;
import java.util.Queue;

public class ChopstickVault<T> {

    private final Queue<T> queue;
    private final int limit;

    public ChopstickVault(int limit){
        this.limit = limit;
        this.queue = new LinkedList<>();
    }

    public synchronized void startStock(T data){
        if(queue.size() == limit){
            System.out.println("dei notify");
            notifyAll();
        }
        queue.offer(data);
    }

    public synchronized void add(T data){
        if(data == null){
            return;
        }
        queue.offer(data);
        notifyAll();
    }

    public synchronized T remove() {
        T result;
        while (queue.isEmpty()){
            try {
                System.out.println("DEADLOCK BITCH");
                wait();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        result = queue.remove();
        notifyAll();
        return result;
    }

    public int getSize(){
        return queue.size();
    }
    public int getLimit(){
        return limit;
    }
}
