import Philosophers.Chopstick;
import Philosophers.ChopstickVault;
import Philosophers.Philosopher;

public class Main {

    public static void main(String[] args) {
        ChopstickVault<Chopstick> vault = new ChopstickVault<>(1);


        Philosopher p1 = new Philosopher(vault,5);
        Thread t1 = new Thread(p1);
        t1.setName("Plato");

        Philosopher p2 = new Philosopher(vault,5);
        Thread t2 = new Thread(p2);
        t2.setName("Apulieus");

        for (int i = 0; i <= vault.getLimit(); i++) {
            vault.startStock(new Chopstick());
        }

        t1.start();
        t2.start();




    }
}
