package Manager;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileManager {

    private static final String FILEPATH = "resources/credentials.txt";

    public String readFile(){

        return null;
    }

    public void writeToFile(String username, String password){

        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(FILEPATH));
            writer.write(username + " " + password);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
