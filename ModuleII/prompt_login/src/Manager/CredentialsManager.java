package Manager;

import User.User;

import java.util.HashMap;
import java.util.Map;

public class CredentialsManager {

    private final Map<String, User> credentialsMap;

    public static CredentialsManager credentialsManager = new CredentialsManager();
    public static FileManager fileManager = new FileManager();

    public CredentialsManager(){
        credentialsMap = new HashMap<>();
    }

    public boolean storeCredentials(String userName, String password) {
        if(credentialsMap.containsKey(userName)){
            return false;
        }
        credentialsMap.put(userName, new User(password));
        fileManager.writeToFile(userName,password);
        return true;
    }

    public User authenticate(String userName, String password){
        User user = credentialsMap.get(userName);
        if(user == null){
            return null;
        }
        if(user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
