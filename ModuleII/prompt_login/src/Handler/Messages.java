package Handler;

public class Messages {

    public static final String WELCOME = "Welcome to HackerDen. Please select your option.";

    public static final String INVALID_USERNAME_PASSWORD = "Invalid Username or Password. Please try again.";

    public static final String USERNAME_ALREADY_EXISTS = "Username already exists. Please choose another one!";

    public static final String CONFIRM_PASSWORD = "Please confirm your password: ";

    public static final String INVALID_OPTION = "Invalid Option.";

    public static final String REGISTER_ACCOUNT = "Please introduce your desired username and your password.";

    public static final String PASSWORDS_DO_NOT_MATCH = "Passwords do not match. Please try again.";

    public static final String LOGIN_SUCCESSFUL = "Login successful. Welcome back ";

    public static final String REGISTER_SUCCESSFUL = "Thank you for registering!";
}
