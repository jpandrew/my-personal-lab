import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;
import java.util.StringTokenizer;

public class SimpleWebServer {



    static final File WEB_ROOT = new File(".");
    static final String DEFAULT_FILE = "index.html";
    static final String FILE_NOT_FOUND = "404.html";
    static final String METHOD_NOT_SUPPORTED = "not_supported.html";
    public final int PORT_NUMBER = 42069;


    static final boolean verbose = true;

    private Socket connect;


    public SimpleWebServer(Socket connect) {
        try {
            initServer();
        } catch (SocketException ex) {

            System.err.println("Error: Could not accept incoming packets: " + ex.getMessage());

        } catch (IOException ex) {
            System.err.println("Error: Network failure: " + ex.getMessage());
            System.exit(1);
        }
    }

    private void initServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(PORT_NUMBER);
        System.out.println("Web Server initialized in port number: " + PORT_NUMBER);
        connect = serverSocket.accept();
        System.out.println("Connected to : " + serverSocket.getInetAddress() + " at (" + new Date() + ")");
    }

    public void start() throws IOException {
        BufferedReader inReader;
        PrintWriter outWriter;
        BufferedOutputStream dataOut;
        String fileRequest;

        try {
            inReader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            outWriter = new PrintWriter(connect.getOutputStream());
            dataOut = new BufferedOutputStream(connect.getOutputStream());

            String input = inReader.readLine();
            StringTokenizer parse = new StringTokenizer(input);
            String method = parse.nextToken().toUpperCase();

            fileRequest = parse.nextToken().toLowerCase();
        }
    }
    private void restart() throws IOException {

        closeClient();
        // block waiting for a client to connect
        System.out.println("INFO: Waiting for a client connection.");
        clientSocket = serverSocket.accept();

        // handle client connection
        System.out.println("INFO: Client accepted: " + clientSocket);
        initServer();
    }

    public void closeClient() throws IOException {
        clientSocket.close();
        serverSocket.close();
        bufferedReader.close();
        outputStreamWriter.close();
    }
    private void splitRequest(String request) throws IOException {
        String[] requestLines = request.split("(\r|\n)");
        String[] requestLine = requestLines[0].split(" ");
        String httpVerb;
        String resourcePath;
        String httpVersion;
        String response = null;
        File resource = null;

        if (requestLine.length == 3) {
            httpVerb = requestLine[0];
            resourcePath = requestLine[1];
            httpVersion = requestLine[2];

            resourcePath = resourcePath.substring(1);

            resource = new File(resourcePath);

            switch (httpVerb) {
                case "GET":
                    if (!isFileValid(resource)) {
                        System.out.println("Error 404: File Not Found.");
                        sendResponse(NOT_FOUND, StatusCode.NOT_FOUND);
                    } else {
                        sendResponse(resource, StatusCode.OK);
                    }
            }
        }
    }


    private void sendResponse(File resource, StatusCode statusCode) throws IOException {
        byte[] content = readFile(resource);

        String responseHeaders = buildHeaders(resource, content, statusCode);

        byte[] responseBytes = responseHeaders.getBytes();

        outputStream.write(responseBytes);
        System.out.println("INFO: Uploading resource: " + resource.getName());
        outputStream.write(content, 0, content.length);
        outputStream.flush();
    }

    private String buildHeaders(File resource, byte[] content, StatusCode statusCode) {
        StringBuilder headers = new StringBuilder();

        headers.append("HTTP/1.1").append(statusCode.getCode()).append(statusCode).append("\r\n");
        headers.append("Content-Type: " + getFileType(resource) + ((getFileType(resource).equals("text")) ? ";charset=UTF-8 \r\n" : "\r\n"));
        headers.append("Content-Length: " + content.length + "\r\n");
        headers.append("\r\n");

        return headers.toString();
    }

    private String getFileType(File resource) {
        String[] resource_split = resource.getName().split("/");
        String fileName = resource_split[resource_split.length - 1];
        String extension = fileName.split("\\.")[1];

        switch (extension) {
            case "txt":
                return "text/plain";
            case "html":
                return "text/html";
            default:
                return "Error choosing extension";
        }
    }

    private byte[] readFile(File resource) {
        BufferedInputStream fileInput;
        byte[] fileToBytes = new byte[(int) resource.length()];
        int bytesRead = 0;

        if (!isFileValid(resource)) {
            System.err.println("ERROR: File provided is not valid.");
        }
        try {
            fileInput = new BufferedInputStream(new FileInputStream(resource.getPath()));
            bytesRead = fileInput.read(fileToBytes, 0, fileToBytes.length);
            System.out.println("INFO: " + bytesRead + " bytes were read.");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("ERROR: " + e.getMessage());
        }
        return fileToBytes;
    }

    public boolean isFileValid(File file) {
        return file.exists() && file.canRead() && file.isFile();
    }


}
