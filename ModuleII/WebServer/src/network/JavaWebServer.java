package network;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class JavaWebServer implements Runnable {

    private static final Logger logger = Logger.getLogger(JavaWebServer.class.getName());

    public static final File ROOT = new File("resources/");
    public static final int PORT = 42069;

    private Socket connect;


    public static void main(String[] args) {
      JavaWebServer webServer = new JavaWebServer(new Socket());
      webServer.listen();
    }

    public JavaWebServer(Socket connect){
        this.connect = connect;
    }

    public void listen(){
        try {
            ServerSocket bindSocket = new ServerSocket(PORT);
            System.out.println("Server started.\nListening for connections on port : " + PORT + " ...\n");
            logger.log(Level.INFO, "server bind to " + getAddress(bindSocket));

            serve(bindSocket);

        } catch (IOException e) {
            System.err.println("Server Connection error : " + e.getMessage());
            logger.log(Level.SEVERE, "could not bind to port " + PORT);
            logger.log(Level.SEVERE, e.getMessage());

            System.exit(1);
        }
    }

    private void serve(ServerSocket bindSocket) throws IOException {

        while (true) {
            connect = bindSocket.accept();
            JavaWebServer myServer = new JavaWebServer(connect);

            System.out.println("Connection opened. (" + new Date() + ") to client: " + getAddress(connect));
            logger.log(Level.INFO, "server bind to " + getAddress(bindSocket));

            Thread thread = new Thread(myServer);
            thread.start();
        }
    }
    @Override
    public void run() {

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            DataOutputStream out = new DataOutputStream(connect.getOutputStream());

                String requestHeaders = fetchRequestHeaders(in);
                if (requestHeaders.isEmpty()) {
                    close(connect);
                    return;
                }

                String request = requestHeaders.split("\n")[0]; // request is first line of header
                String httpVerb = request.split(" ")[0]; // verb is the first word of request
                String resource = request.split(" ").length > 1 ? request.split(" ")[1] : null; // second word of request

                logger.log(Level.INFO, "Request received: " + request);
                logger.log(Level.FINE, "Headers : \n" + requestHeaders);

                if (!httpVerb.equals("GET")) {
                    logger.log(Level.WARNING, "request not supported from " + getAddress(connect));
                    reply(out, HTTPHelper.notAllowed());
                    close(connect);
                    return;

                }

                if (resource == null) {
                    logger.log(Level.WARNING, "resource not specified from " + getAddress(connect));
                    reply(out, HTTPHelper.badRequest());
                    close(connect);
                    return;
                }

                String filePath = getPathForResource(resource);
                if (!HTTPMedia.isSupported(filePath)) {
                    logger.log(Level.WARNING, "request for content type not supported from " + getAddress(connect));
                    reply(out, HTTPHelper.unsupportedMedia());
                    close(connect);
                    return;
                }

                File file = new File(filePath);
                if (file.exists() && !file.isDirectory()) {

                    reply(out, HTTPHelper.ok());

                } else {

                    logger.log(Level.WARNING, file.getPath() + " not found");
                    reply(out, HTTPHelper.notFound());
                    filePath = JavaWebServer.ROOT + "404.html";
                    file = new File(filePath);

                }

                reply(out, HTTPHelper.contentType(filePath));
                reply(out, HTTPHelper.contentLength(file.length()));

                streamFile(out, file);
                close(connect);

            } catch (SocketException ex) {

                logger.log(Level.INFO, "client disconnected " + getAddress(connect));

            } catch (IOException ex) {

                logger.log(Level.WARNING, ex.getMessage());
                close(connect);
            }
    }

    private String fetchRequestHeaders(BufferedReader in) throws IOException {

        String line;
        StringBuilder builder = new StringBuilder();

        // read the full http request
        while ((line = in.readLine()) != null && !line.isEmpty()) {
            builder.append(line).append("\n");
        }

        return builder.toString();

    }

    private String getPathForResource(String resource) {

        String filePath = resource;

        Pattern pattern = Pattern.compile("(\\.[^.]+)$"); // regex for file extension
        Matcher matcher = pattern.matcher(filePath);

        if (!matcher.find()) {
            filePath += "/index.html";
        }

        filePath = JavaWebServer.ROOT + filePath;

        return filePath;
    }

    private void reply(DataOutputStream out, String response) throws IOException {
        out.writeBytes(response);
    }

    private void streamFile(DataOutputStream out, File file) throws IOException {

        byte[] buffer = new byte[1024];
        FileInputStream in = new FileInputStream(file);

        int numBytes;
        while ((numBytes = in.read(buffer)) != -1) {
            out.write(buffer, 0, numBytes);
        }

        in.close();

    }

    private void close(Socket clientSocket) {

        try {

            logger.log(Level.INFO, "closing client socket for " + getAddress(clientSocket));
            clientSocket.close();

        } catch (IOException e) {

            logger.log(Level.INFO, e.getMessage());
        }

    }


    private String getAddress(ServerSocket socket) {

        if (socket == null) {
            return null;
        }

        return socket.getInetAddress().getHostAddress() + ":" + socket.getLocalPort();
    }

    private String getAddress(Socket socket) {
        return socket.getInetAddress().getHostAddress() + ":" + socket.getLocalPort();
    }


}

