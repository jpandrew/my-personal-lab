package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.utils.Security;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MockUserServiceJDBC implements UserService {

    private Connection dbConnection = null;

    public MockUserServiceJDBC(){
        getDbConnection();
    }

    public void getDbConnection() {
        try {
            if (dbConnection == null) {
                dbConnection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/ac?useSSL=false", "ricardo", Security.MYPASS);
            }
        } catch (SQLException ex) {
            System.out.println("Failure to connect to database: " + ex.getMessage());
        }
    }

    public void close() {
        try {
            if (dbConnection != null) {
                dbConnection.close();
            }
        } catch (SQLException ex) {
            System.out.println("Failure to close database connection: " + ex.getMessage());
        }
    }

    @Override
    public boolean authenticate(String username, String password) {
        try {

            String query = "SELECT username, password FROM user WHERE username =? AND password=?;";
            PreparedStatement statement = dbConnection.prepareStatement(query);

            statement.setString(1,username);
            statement.setString(2,Security.getHash(password));

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                return false;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void add(User user) {

        try {

            String query = "INSERT INTO user(username, password, email, firstname, lastname, phone) " +
                    "VALUES(?,?,?,?,?,?)";

            PreparedStatement statement = dbConnection.prepareStatement(query);

            statement.setString(1,user.getUsername());
            statement.setString(2,user.getPassword());
            statement.setString(3,user.getEmail());
            statement.setString(4,user.getFirstName());
            statement.setString(5,user.getLastName());
            statement.setString(6,user.getPhone());

            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.err.println("Error adding user.");
            return;
        }
        System.out.println("User added successfully");
    }

    @Override
    public User findByName(String username) {
        User user = null;
        try {

            String query = "SELECT * FROM user WHERE username =?;";
            PreparedStatement statement = dbConnection.prepareStatement(query);
            statement.setString(1,username);


            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                String usernameValue = resultSet.getString("username");
                String passwordValue = resultSet.getString("password");
                String emailValue = resultSet.getString("email");
                String firstNameValue = resultSet.getString("firstname");
                String lastNameValue = resultSet.getString("lastname");
                String phoneValue = resultSet.getString("phone");

                user = new User(usernameValue,emailValue,passwordValue,firstNameValue,lastNameValue,phoneValue);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        User user;
        Statement statement;
        List<User> list = new ArrayList<>();
        try {
            statement = dbConnection.createStatement();

            String query = "SELECT * FROM user;";

            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String usernameValue = resultSet.getString("username");
                String passwordValue = resultSet.getString("password");
                String emailValue = resultSet.getString("email");
                String firstNameValue = resultSet.getString("firstname");
                String lastNameValue = resultSet.getString("lastname");
                String phoneValue = resultSet.getString("phone");

                user = new User(usernameValue,emailValue,passwordValue,firstNameValue,lastNameValue,phoneValue);
                list.add(user);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return list;
    }

    @Override
    public int count() {
        int result = 0;

        try {
            Statement statement = dbConnection.createStatement();

            String query = "SELECT COUNT(*) FROM user";
            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return result;
    }
}
