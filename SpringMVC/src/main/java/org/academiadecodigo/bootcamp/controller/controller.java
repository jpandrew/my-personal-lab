package org.academiadecodigo.bootcamp.controller;

import org.academiadecodigo.bootcamp.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

@Controller
public class controller {
    // Map the URL/Verb to the method
    @RequestMapping(method = RequestMethod.GET, value = "/hello")
    public String sayHello(Model model) {
        User user = new User("Joao", "gay@gaymail.com");
        // Add an attribute to the request
        model.addAttribute("user", user);

        // Return the view name
        return "hello";

    }
}

